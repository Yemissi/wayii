	<?php 
		
		// Template Name: Porto-Novo
		
	?>
	
	<div class="container">	
		
		<div style="background-image:linear-gradient(180deg, rgba(56, 54, 54, 0.25) 0%, rgba(36, 34, 34, 0.5) 100%), url(<?php echo  get_field("background_image"); ?>); background-positon: center; background-size:cover; background-repeat: no-repeat;">
			<?php get_header() ;?>
		
			<?php 
				$args = array(
				'post_type' => 'post',
				'posts_per_page' => 1,
				'category_name' => 'porto-novo',
				'order' => 'DESC',
				);
				
				$myquery = new WP_Query( $args );
				if($myquery->have_posts()) : 
					while($myquery->have_posts()) : 
						$myquery->the_post();?>
						
					<div class="marge">		
						<p class="couleur">
							<a href="<?php the_permalink(); ?>" class="date">
								Publié le : <?php the_date(); ?> 	
							</a>	
						</p>
					
						<h1>
							<a href="<?php the_permalink(); ?>" class="card-text size-2 font-dancing text-white">
								
								<?php the_title(); ?>
							</a>						
						</h1>
							
						<p class="mt-4">
							<a href="<?php the_permalink(); ?>" class="text-white">
								<?php the_excerpt(); ?>
							</a>	
						</p>
						
						<p>
						<a href="<?php the_permalink(); ?>" class="btn btn-orange rounded-pill px-4 py-2 mt-4">Lire l’article</a>
						</p>
					</div>
			<?php 
			endwhile; 
			endif; 
			?>
			
		</div>
	
		<h5 class="mt-4">
			Destination/Porto-Novo
		</h5>
		
	</div>
	<!-- article -->
	<div class="container card-article">
		<h2 class="color-orange text-center mb-5">Articles</h2>
		
		<div class="row">
		
			<?php 
			$args = array(
			'post_type' => 'post',
			'category_name' => 'porto-novo',
			'order' => 'ASC',
			
			);
			
			$myquery = new WP_Query( $args );
			if($myquery->have_posts()) : 
				while($myquery->have_posts()) : 
					$myquery->the_post(); ?>
			
			<div class="col">
				<div class="card article text-center border-0">
					<p>
						<a href="<?php the_permalink(); ?>" class="date">
							<?php the_date(); ?> 	
						</a>	
					</p>
					
					<p>
						<a href="<?php the_permalink(); ?>" class="image">
							<?php the_post_thumbnail(); ?>
						</a>	
					</p>
					
					<p class="catégorie"><?php the_category(); ?></p>
					
					<p class="title">
						<a href="<?php the_permalink(); ?>" class="card-text size-2 font-dancing">
							
							<?php the_title(); ?>
						</a>
						
					</p>
				</div>
			</div>
			<?php endwhile; endif; ?>
		</div>
	</div>
<?php get_footer() ;?>