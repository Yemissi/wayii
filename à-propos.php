<?php 

// Template Name: A propos

 get_header() ;?>
 
	<?php
	
        $header_title = get_field("header_title");
        $body_title = get_field("body_title");
		$body_structure = get_field("body_structure");
        $body_structure_title1 = $body_structure["body_structure_title1"];
        $body_structure_description1 = $body_structure["body_structure_description1"];
		$body_structure_title2 = $body_structure["body_structure_title2"];
        $body_structure_description2 = $body_structure["body_structure_description2"];
		$body_structure_title3 = $body_structure["body_structure_title3"];
        $body_structure_description3 = $body_structure["body_structure_description3"];
		$profil = get_field("profil");
        $profil_image = $profil["profil_image"];
		$profil_name = $profil["profil_name"];
		$profil_profession = $profil["profil_profession"];
        
	?>
    <!-- fond d'ecran -->
    <div style="background-image:url(<?php echo  get_field("background_image"); ?>);">
        
        <section class="text-white text-center py-5">
            <h1 class="py-5"><?php echo  $header_title; ?><!--A propos--></h1>
        </section>
    </div>
    <div class="container my-5">
        <div class="row">
            <section class="col-sm-8">
                <h5 class="color-orange py-2"><?php echo  $body_title; ?><!--A propos--></h5>
                <h4><?php echo  $body_structure_title1; ?><!--Pourquoi Wayii?--></h4>
                <p><?php echo  $body_structure_description1; ?><!--Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque totam dolorum cumque soluta, unde eos corporis esse perspiciatis eum nemo animi quas amet nobis minus ullam! Reiciendis ducimus iusto accusamus.
                Voluptatibus modi quasi, ab voluptas fugiat repellendus architecto, culpa, excepturi assumenda illo sit provident illum dolores quibusdam ex ipsum sint aperiam maiores quaerat unde eveniet doloribus! Modi illum numquam sit!
                Adipisci, ex dolorum? Totam reprehenderit error suscipit tempora iusto dolore officia! <br> Velit, nostrum! Repellendus distinctio expedita laudantium magni quaerat. Sunt minima possimus ducimus expedita voluptatum sapiente ipsam sit, rem ab. --></p>
                <h4><?php echo  $body_structure_title2; ?><!--Découvrez de magnifiques destinations--></h4>
                <p><?php echo  $body_structure_description3; ?><!--Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque totam dolorum cumque soluta, unde eos corporis esse perspiciatis eum nemo animi quas amet nobis minus ullam! Reiciendis ducimus iusto accusamus.
                Voluptatibus modi quasi, ab voluptas fugiat repellendus architecto, culpa, excepturi assumenda illo sit provident illum dolores quibusdam ex ipsum sint aperiam maiores quaerat unde eveniet doloribus! Modi illum numquam sit!
                Adipisci, ex dolorum? Totam reprehenderit error suscipit tempora iusto dolore officia! <br> Velit, nostrum! Repellendus distinctio expedita laudantium magni quaerat. Sunt minima possimus ducimus expedita voluptatum sapiente ipsam sit, rem ab. --></p>
                <h4><?php echo  $body_structure_title3; ?><!--Découvrez des astuces--></h4>
                <p><?php echo  $body_structure_description3; ?><!--Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque totam dolorum cumque soluta, unde eos corporis esse perspiciatis eum nemo animi quas amet nobis minus ullam! Reiciendis ducimus iusto accusamus.
                Voluptatibus modi quasi, ab voluptas fugiat repellendus architecto, culpa, excepturi assumenda illo sit provident illum dolores quibusdam ex ipsum sint aperiam maiores quaerat unde eveniet doloribus! Modi illum numquam sit!
                Adipisci, ex dolorum? Totam reprehenderit error suscipit tempora iusto dolore officia! <br> Velit, nostrum! Repellendus distinctio expedita laudantium magni quaerat. Sunt minima possimus ducimus expedita voluptatum sapiente ipsam sit, rem ab. --></p>
             </section>
             <div class="col-sm-4 mt-5">
                <div class="card border-0 mt-3 px-3">
                    <img src="<?php echo  $profil_image; ?>" class="card-img-top" alt="CEO"><!--images/img-article/edna.png-->
                    <div class="card-body">
                      <h5 class="card-title text-center"><?php echo  $profil_name; ?><!--Edna HENNOU--></h5>
                      <p class="card-text text-center"><?php echo  $profil_profession; ?><!--Rédactrice web <br> Passionnée de voyages--></p>
					  <div class="list-inline d-flex justify-content-center">
                        <li class="me-4"><a href=""><i class="icofont-instagram icon-size color-orange"></i></a></li>
                        <li class="me-4"><a href=""><i class="icofont-facebook social color-orange"></i></a></li> 
                        <li><a href=""><i class="icofont-twitter social color-orange"></i></a></li> 
					  </div>
                    </div>
                  </div>
             </div>
        </div>
    </div>
<?php get_footer() ;?>