<div id="commentaires" class="comments">
    <?php if ( have_comments() ) : ?>
        <h2 class="comments__title">
            <?php echo get_comments_number(); // Nombre de commentaires ?> Commentaire(s)
        </h2>
    
        <ol class="comment__list">
            <?php
            	// La fonction qui liste les commentaires
                wp_list_comments( array(
                    'style'       => 'ol',
                    'short_ping'  => true,
                    'avatar_size' => 74,
                ) );
            ?>
        </ol>
        
    <?php 
    	// S'il n'y a pas de commentaires
    	else : 
    ?>
    
    <?php endif; ?>
 
    <?php comment_form(); // Le formulaire d'ajout de commentaire ?>
	
	<script>
       
		document.getElementById("comment").placeholder = "Ajouter un commentaire";
		document.getElementById("comment").cols = "50";
		document.getElementById("comment").rows = "4";
		document.getElementById("submit").value = "Commenter";
	
    </script>
</div>