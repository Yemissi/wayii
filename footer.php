<?php wp_footer(); ?>

	<div class="footer">
        <div class="container text-white">
            <div class="row mb-3">
                <div class="col-md-3">            
                    <h3 class="mb-3">Destinations</h3>
                    <p class="color-tranp margin"><a href="http://localhost/wayii/porto-novo/" class="text-white">Porto-novo</a></p>
                    <p class="color-tranp margin"><a href="http://localhost/wayii/dassa/" class="text-white">Dassa</a></p>
                    <!-- <p class="color-tranp margin">Grand-popo</p>
                    <p class="color-tranp margin">Ganvié</p>
                    <p class="color-tranp">Allada</p> -->
                </div>
                <div class="col-md-6">
                    <h3 class="text-center mb-3">Newsletters</h3>
                    <p class="text-center color-tranp">Soyez informez à temps de nos nouvelles publications</p>
                    
					<?php get_template_part('template-parts/content', 'newsletter'); ?>
                </div>
                <div class="col-md-3">
                    <h3 class="mb-3">Contactez-nous</h3>
                    <p class="color-tranp">wayiibenin@gmail.com</p>
                    <p class="color-tranp">(229) 67 04 83 93</p>
                    <div class="list-inline d-flex text-white">
                        <li class="me-4"><a href=""><i class="icofont-instagram icon-size"></i></a></li>
                        <li class="me-4"><a href=""><i class="icofont-facebook social-icon"></i></a></li> 
                        <li><a href=""><i class="icofont-twitter social-icon"></i></a></li> 
                    </div>
                </div>
            </div>
        </div>
        <hr class="color-gray">
        <p class="text-center padding-bottom text-white pb-4">Wayii copyright @ 2021. Tout droits réservés</p>
    </div>

</body>

</html>