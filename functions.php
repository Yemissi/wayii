<?php

/* permet d'ajouter une image en avant*/
add_theme_support('post-thumbnails');

add_theme_support('title-tag');

//relier boostrap js et css à mon projet

function wayii_styles(){
	wp_enqueue_style(
	"boostrap-css",
	get_template_directory_uri() . '/css/bootstrap.min.css',
	array(),
	true
	);
	
	wp_enqueue_style(
	"boostrap1-css",
	get_template_directory_uri() . '/css/bootstrap.css',
	array(),
	true
	);
	
	wp_enqueue_style(
	"boostrap2-css",
	get_template_directory_uri() . '/css/bootstrap-utilities.css',
	array(),
	true
	);
	
	wp_enqueue_style(
	"boostrap3-css",
	get_template_directory_uri() . '/css/bootstrap-reboot.css',
	array(),
	true
	);
	
	wp_enqueue_style(
	"boostrap4-css",
	get_template_directory_uri() . '/css/bootstrap-grid.css',
	array(),
	true
	);
 
	wp_enqueue_style(
	"style-css",
	get_template_directory_uri() . '/css/style.css',
	array(),
	true
	);
	
	wp_enqueue_style(
	"style-a-propos-css",
	get_template_directory_uri() . '/css/style-a-propos.css',
	array(),
	true
	);
	
	wp_enqueue_style(
	"style-article-css",
	get_template_directory_uri() . '/css/style-article.css',
	array(),
	true
	);
	
	wp_enqueue_style(
	"style-astuces-css",
	get_template_directory_uri() . '/css/astuces.css',
	array(),
	true
	);
	
	wp_enqueue_style(
	"style-contact-css",
	get_template_directory_uri() . '/css/style-contact.css',
	array(),
	true
	);

	wp_enqueue_style(
	"icofont-css",
	get_template_directory_uri() . '/icofont/icofont.css',
	array(),
	true
	);
	
	wp_enqueue_style(
	"style",
	get_stylesheet_uri(),
    array(),
    '1.0'
	);
}

add_action("wp_enqueue_scripts", "wayii_styles");

function wayii_scripts(){
    wp_enqueue_script(
    "boostrap-js",
    get_template_directory_uri() . '/js/bootstrap.min.js',
    array(),
    true
 );
 
 wp_enqueue_script(
    "boostrap1-js",
    get_template_directory_uri() . '/js/bootstrap.js',
    array(),
    true
 );
 
 wp_enqueue_script(
    "bootstrap-bundle-js",
    get_template_directory_uri() . '/js/bootstrap.bundle.js',
    array(),
    true
 );
 
 wp_enqueue_script(
    "popper-js",
    get_template_directory_uri() . '/js/popper.min.js',
    array(),
    true
 );
}

add_action('wp_enqueue_scripts', 'wayii_scripts');

// création de menu dans apparance et Main c'est le ID du menu
register_nav_menu('main', 'Menu Principal');

function google_fonts() {
 
wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css2?famille=Danse+Script:wght@400;500;600;700&display=swap', true ); 
}
 
add_action( 'wp_enqueue_scripts', 'google_fonts' );



