	<div class="row">
        <div class="col-md-8">
            <label for="inputEmail4" class="form-label"></label>
            <input type="email" class="form-control color-gray" placeholder="Ex: name@gmail.com" id="inputEmail4">
        </div>
        <div class="col-md-4">
            <button type="button" class="btn btn-orange rounded px-4 mt-4">Rejoindre</button>
        </div>
    </div>
    <div class="mb-3 form-check mt-2">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label size color-tranp" for="exampleCheck1">En cliquant, vous acceptez la politique de confidentialité</label>
    </div>