	<?php 

	$article_header=get_field("article_header");
	$article_header_author=$article_header["article_header_author"];
	$article_header_name=$article_header["article_header_name"];
	
	$article_profil=get_field("article_profil");
	$article_profil_bg_img=$article_profil["article_profil_bg_img"];
	$article_profil_author=$article_profil["article_profil_author"];
	$article_profil_description=$article_profil["article_profil_description"];

	?>
	
	<div style="background-image: linear-gradient(180deg, rgba(56, 54, 54, 0.25) 0%, rgba(36, 34, 34, 0.5) 100%), url(<?php echo get_field("article_background_image"); ?>); background-positon: center; background-size:cover; background-repeat: no-repeat;" class="bg-astuce">

	
		<?php get_header(); ?>
	
		<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>
    
		<h1 class="post__title text-white text-center my-5"><?php the_title(); ?></h1>
      

		<div class="post__content">
			
		</div>
	
	
		<div class="text-white text-center">
            
            <div class="">
                <img src="<?php echo  $article_header_author; ?>" alt="" class="rounded-circle tall-autor"><!--../images/img-article/img-comment.jpg" -->
            </div>
            <h4 class="py-5 fst-italic"><?php echo  $article_header_name; ?><!--Par Amel--></h4>
        </div>
        <div class="conatainer bg-orange">
            <div class="py-3"></div>
        </div>
	</div>
    <div class="container ">
        <div class="row">
            <div class="col-sm-8">
                <div class="card no-border mt-5">
                    <div class="card-body post__content">
						
						<?php the_content(); ?>	
						
                    </div>
                </div>  
            </div>
			
            <div class="col-sm-4 mt-5">
                <div class="card mt-5">
                    <div class="position-relative">
                        <img src="<?php echo  $article_profil_bg_img; ?>" class="card-img-top" alt="fond-persona">
                        <div class="position-absolute top-100 start-50 translate-middle">
                            <img src="<?php echo  $article_profil_author; ?>" alt="persona" class="rounded-circle tall-autor">
                        </div>
                    </div>
                    <div class="card-body">
						<p class="card-text text-center mt-5"><?php echo  $article_profil_description; ?></p>
						<ul class="list-inline d-flex justify-content-center">
							<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/img-article/facebook-logo.png" alt="" class="tall-icon py-3 mx-3 "></a></li>
							<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/img-article/instagram.png" alt="" class="tall-icon py-3  mx-3"></a></li>
							<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/img-article/twitter.png" alt="" class="tall-icon py-3  mx-3"></a></li>
						</ul>
                    </div>
                </div>

				<div class="card text-white text-center mt-5">
                    <div class="card-body bg-form">
						<h4 class="card-title my-2">Newsletter</h4>
						<p class="card-text">Abonne toi et plonge toi <br> dans l'aventure <br> Wayii</p>
						<form class="px-5 my-4 rounded-2">
							<div class="mb-3 px-3">
							  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Adresse e-mail">
							</div>
						</form>
						<a href="#" class="btn rounded-pill btn-warning text-white mb-4">S'inscrire</a>
                    </div>
				</div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                
                    <div class="d-flex mb-4">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/img-article/Line 3.png" alt="" class="tall-trait mt-3 d-none d-lg-block ms-2">
                        <h4 class="text-center mx-sm-5 mt-2">Commentaires</h4>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/img-article/Line 4.png" alt="" class="tall-trait mt-3 d-none d-lg-block">
                    </div>
					<?php comments_template(); // Par ici les commentaires ?>
    
            </div>
        </div>
       
	</div>
</div>

  <?php endwhile; endif; ?>

<?php get_footer() ;?>