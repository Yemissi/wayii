	<?php 
	
		// Template Name: Destination
		
	?>
	
	<div class="container">
	
		<div style="background-image: linear-gradient(180deg, rgba(56, 54, 54, 0.25) 0%, rgba(36, 34, 34, 0.5) 100%), url(<?php echo get_field("destination_bg_image"); ?>); background-positon: center; background-size:cover; background-repeat: no-repeat;">
			<?php get_header() ;?>
		
			<?php 
				$args = array(
				'post_type' => 'post',
				'posts_per_page' => 1,
				'category_name' => 'destination',
				'order' => 'DESC',
				);
				
				$myquery = new WP_Query( $args );
				if($myquery->have_posts()) : 
					while($myquery->have_posts()) : 
						$myquery->the_post();?>
				
				<div class="marge">		
					
					<p class="couleur">
						<a href="<?php the_permalink(); ?>" class="date">
							Publié le : <?php the_date(); ?> 	
						</a>	
					</p>
				
					<h1>
						<a href="<?php the_permalink(); ?>" class="card-text size-2 font-dancing text-white">
							
							<?php the_title(); ?>
						</a>						
					</h1>
					
					<p class="mt-4">
						<a href="<?php the_permalink(); ?>" class="text-white">
							<?php the_excerpt(); ?>
						</a>	
					</p>
					
					<p>
					<a href="<?php the_permalink(); ?>" class="btn btn-orange rounded-pill px-4 py-2 mt-4">Lire l’article</a>
					</p>
				</div>
				
			<?php 
			endwhile; 
			endif; 
			?>
			 
		</div>
		
		<h5 class="mt-4">
			Destination
		</h5>
		
		<!--<div class="container header-content">
			<div class="overlay">
				<img src="<?php echo get_template_directory_uri(); ?>/images/accueil/pexels-kamaji-ogino-5064883.jpg" class="d-block w-100" alt="...">
			</div>
			<div class="centered-left text-white">
				<p class="mt-4 color-orange">Publié le 20 septembre 2020</p>
				<h1 class="font-dancing">Comment économiser pendant votre voyage ?</h1>
				<p class="mt-4">Vous voulez planifier pour une première fois un projet de voyage? Pas de panique!</p>
				<a href="article-astuce/article-astuce1.html" class="btn btn-orange rounded-pill px-4 py-2 mt-4">Lire l’article</a>
			</div> 
			<h5 class="mt-4">
				Astuces de voyage
			</h5>  
		</div>-->

		<!-- article -->
		<div class="card-article">
			<h2 class="color-orange text-center mb-5">Articles</h2>
			
			<div class="row">
			
				<?php 
				$args = array(
				'post_type' => 'post',
				'category_name' => 'destination',
				'order' => 'ASC',
				'orderby' => 'date',
				);
				
				$myquery = new WP_Query( $args );
				if($myquery->have_posts()) : 
					while($myquery->have_posts()) : 
						$myquery->the_post(); ?>
				
				<div class="col">
					<div class="card article text-center border-0">
						<p>
							<a href="<?php the_permalink(); ?>" class="date">
								<?php the_date(); ?> 	
							</a>	
						</p>
						
						<p>
							<a href="<?php the_permalink(); ?>" class="image">
								<?php the_post_thumbnail(); ?>
							</a>	
						</p>
						
						<p class="catégorie"><?php the_category(); ?></p>
						
						<p class="title">
							<a href="<?php the_permalink(); ?>" class="card-text size-2 font-dancing">
								
								<?php the_title(); ?>
							</a>
							
						</p>
					</div>
				</div>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</div>
<?php get_footer() ;?>