	<div class="container">
		<?php get_header() ;?>
	
		<?php 
			$args = array(
			'post_type' => 'post',
			'posts_per_page' => 1,
			'order' => 'DESC',
			);
			
			$myquery = new WP_Query( $args );
			if($myquery->have_posts()) : 
				while($myquery->have_posts()) : 
					$myquery->the_post();?>
					
				<p>
				<a href="<?php the_permalink(); ?>" class="date">
					Publié le : <?php the_date(); ?> 	
				</a>	
				</p>
			
				<p class="title">
					<a href="<?php the_permalink(); ?>" class="card-text size-2 font-dancing">
						
						<?php the_title(); ?>
					</a>						
				</p>
				
				<p>
				<a href="<?php the_permalink(); ?>" class="btn btn-orange rounded-pill px-4 py-2 mt-4">Lire l’article</a>
				</p>
		
		<?php 
		endwhile; 
		endif; 
		?>
		
		<h5 class="mt-4">
			Astuces de voyage
		</h5> 
	</div>
		<!--<div class="container header-content">
            <div class="overlay">
                <img src="<?php echo get_template_directory_uri(); ?>/images/accueil/pexels-kamaji-ogino-5064883.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="centered-left text-white">
                <p class="mt-4 color-orange">Publié le 20 septembre 2020</p>
                <h1 class="font-dancing">Comment économiser pendant votre voyage ?</h1>
                <p class="mt-4">Vous voulez planifier pour une première fois un projet de voyage? Pas de panique!</p>
                <a href="article-astuce/article-astuce1.html" class="btn btn-orange rounded-pill px-4 py-2 mt-4">Lire l’article</a>
            </div> 
            <h5 class="mt-4">
                Astuces de voyage
            </h5>  
        </div>-->

        <!-- article -->
        <div class="container card-article">
            <h2 class="color-orange text-center mb-5">Articles</h2>
            
            <div class="row">
			
				<?php 
				$args = array(
				'post_type' => 'post',
				'category_name' => 'astuces',
				'order' => 'ASC',
				'orderby' => 'date',
				);
				
				$myquery = new WP_Query( $args );
				if($myquery->have_posts()) : 
					while($myquery->have_posts()) : 
						$myquery->the_post();?>
				
				<div class="col">
					<div class="card article text-center border-0">
						<p>
							<a href="<?php the_permalink(); ?>" class="date">
								<?php the_date(); ?> 	
							</a>	
						</p>
						
						<p>
							<a href="<?php the_permalink(); ?>" class="image">
								<?php the_post_thumbnail(); ?>
							</a>	
						</p>
						
						<p class="catégorie"><?php the_category(); ?></p>
						
						<p class="title">
							<a href="<?php the_permalink(); ?>" class="card-text size-2 font-dancing">
								
								<?php the_title(); ?>
							</a>
							
						</p>
					</div>
				</div>
				<?php endwhile; endif; ?>
			</div>
		</div>
<?php get_footer() ;?>