<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <?php wp_body_open(); ?>

    <nav class="header navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
			<a class="navbar-brand" href="http://localhost/wayii/">
				<img src="<?php echo get_template_directory_uri(); ?>/images/img-article/logo.png" alt="Logo" width="100" height="80">
			</a>
			
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
			<div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
				<?php 
					wp_nav_menu(
						array(
							'theme_location' => 'main',
							'container' => 'ul',
							'menu_class' => 'my-menu',
						)
					); 
				
				?>
			</div>
		</div>
    </nav>