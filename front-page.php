<?php get_header() ;?>
<?php

	$galerie_1 = get_field("galerie_1");
	$galerie1_modal = get_field("galerie1_modal");
	$galerie1_modal1 = $galerie1_modal["galerie1_modal1"];
	$galerie1_modal2 = $galerie1_modal["galerie1_modal2"];
	$galerie2 = get_field("galerie2");
	$galerie2_modal = get_field("galerie2_modal");
	$galerie2_modal1 = $galerie2_modal["galerie2_modal1"];
	$galerie2_modal2 = $galerie2_modal["galerie2_modal2"];
	$galerie3 = get_field("galerie3");
	$galerie3_modal = get_field("galerie3_modal");
	$galerie3_modal1 = $galerie3_modal["galerie3_modal1"];
	$galerie3_modal2 = $galerie3_modal["galerie3_modal2"];
	$galerie4 = get_field("galerie4");
	$galerie4_modal = get_field("galerie4_modal");
	$galerie4_modal1 = $galerie4_modal["galerie4_modal1"];
	$galerie4_modal2 = $galerie4_modal["galerie4_modal2"];
	$slider_1 = get_field("slider_1");
	$slider_2 = get_field("slider_2");
	$slider_3 = get_field("slider_3");
	$slider_text = get_field("slider_text");
	$slider_text_title = $slider_text["slider_text_title"];
	$slider_text_description = $slider_text["slider_text_description"];
	$slider_text_bottom = $slider_text["slider_text_bottom"];
	
?>
<!-- carousel -->

        <div id="carouselExampleCaptions" class="carousel slide carousel-center" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active me-3" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2" class="me-3"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
              <div class="carousel-item active overlay">
                <img src="<?php echo  $slider_1; ?>" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item overlay">
                <img src="<?php echo  $slider_2; ?>" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item overlay">
                <img src="<?php echo  $slider_3; ?>" class="d-block w-100" alt="...">
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
            <div class="centered text-white text-center">
                <h1 class="font-dancing text-center"><?php echo  $slider_text_title; ?></h1>
                <p class="mt-4 text-center"><?php echo  $slider_text_description; ?></p>
                <a href="<?php echo  $slider_text_bottom; ?>" class="btn text-center btn-orange rounded-pill px-4 py-3 mt-4">Qu'est-ce que wayii?</a>
            </div>
        </div>
        
    
    <!-- bienvenu -->
	
	<?php
	
        $image = get_field("image");
        $titre = get_field("titre");
        $description = get_field("description");
		$description_paragraphe_1 = $description["description_paragraphe_1"];
        $description_paragraphe_2 = $description["description_paragraphe_2"];
        $lire_plus = get_field("lire_plus");
		
    ?>
 
    <div class="container bienvenu">
        <div class="row">
            <div class="col-lg-5 mb-3 d-none d-lg-block">
                <div class="bg-img">
                    <img src="<?php echo  $image; ?>" alt="">
                </div>
            </div>
            <div class="col-lg-7 text-center text-size">
                <h1 class="font-dancing"><?php echo  $titre; ?> </h1> 
                <p class="mt-4"> <?php echo  $description_paragraphe_1; ?> 
                </p>
                <p class="mt-2"> <?php echo  $description_paragraphe_2; ?> 
                </p>
                <a href="<?php echo  $lire_plus; ?>" class="btn btn-orange2 rounded-pill mt-4">Lire plus</a>
            </div>
        </div>
    </div>
	
	<!-- article -->

    <div class="container card-article">
            <h2 class="color-orange text-center mb-5">Articles</h2>
            
            <div class="row">
			
				<?php 
				$args = array(
				'post_type' => 'post',
				'posts_per_page' => 3,
				'order' => 'ASC',
				);
				
				$myquery = new WP_Query( $args );
				if($myquery->have_posts()) : 
					while($myquery->have_posts()) : 
						$myquery->the_post();
						?>
				
						<div class="col">
							<div class="card article text-center border-0">
								<p>
									<a href="<?php the_permalink(); ?>" class="date">
										<?php the_date(); ?> 	
									</a>	
								</p>
								
								<p>
									<a href="<?php the_permalink(); ?>" class="image">
										<?php the_post_thumbnail(); ?>
									</a>	
								</p>
								
								<p class="catégorie"><?php the_category(); ?></p>
								
								<p class="title">
									<a href="<?php the_permalink(); ?>" class="card-text size-2 font-dancing">
										
										<?php the_title(); ?>
									</a>
									
								</p>
							</div>
						</div>
				<?php 
				endwhile; 
				endif; 
				?>
			</div>
		</div>
	
	<!-- Galerie -->

    <div id="menu-galerie" class="galerie">
        <div class="py-4 container">
            <div>
                <h2 class="text-center underline"><span class="text-white">Visitez notre </span>Galerie</h2>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 mt-3 mt-md-3 mt-sm-3 galerie-image">
                    <img src="<?php echo  $galerie_1; ?>" alt="" class="w-100" height="400px"><!--images/accueil/pexels-picha-6211210.jpg-->
                    <div class="modal fade" id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalToggleLabel">Image 1</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <img src="<?php echo  $galerie1_modal1; ?>" alt="" class="w-100 h-100"><!--images/accueil/pexels-picha-6210510.jpg-->
                            </div>
                            <div class="modal-footer">
                              <button class="btn btn-orange3 rounded px-4 py-3" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal" data-bs-dismiss="modal">Open second image</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="modal fade" id="exampleModalToggle2" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalToggleLabel2">Image 2</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <img src="<?php echo  $galerie1_modal2; ?>" alt="" class="w-100 h-100"><!--images/accueil/pexels-nishant-aneja-2385654.jpg-->
                            </div>
                            <div class="modal-footer">
                              <button class="btn btn-orange3 rounded px-4 py-3" data-bs-target="#exampleModalToggle" data-bs-toggle="modal" data-bs-dismiss="modal">Back to first</button>
                            </div>
                          </div>
                        </div>
                    </div>
                    <a class="btn position" data-bs-toggle="modal" href="#exampleModalToggle" role="button"><img src="<?php echo get_template_directory_uri(); ?>/images/accueil/fluent_dual-screen-group-24-regular.svg" alt=""></a>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 mt-3 mt-md-3 mt-sm-3 galerie-image">
                    <img src="<?php echo  $galerie2; ?>" alt="" class="w-100" height="400px"><!--images/accueil/pexels-lisa-5653734.jpg-->
                    <div class="modal fade" id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalToggleLabel">Image 1</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <img src="<?php echo  $galerie2_modal1; ?>" alt="" class="w-100 h-100"><!--images/accueil/pexels-picha-6210510.jpg-->
                            </div>
                            <div class="modal-footer">
                              <button class="btn btn-orange3 rounded px-4 py-3" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal" data-bs-dismiss="modal">Open second image</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="modal fade" id="exampleModalToggle2" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalToggleLabel2">Image 2</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <img src="<?php echo  $galerie2_modal2; ?>" alt="" class="w-100 h-100"><!--images/accueil/pexels-nishant-aneja-2385654.jpg-->
                            </div>
                            <div class="modal-footer">
                              <button class="btn btn-orange3 rounded px-4 py-3" data-bs-target="#exampleModalToggle" data-bs-toggle="modal" data-bs-dismiss="modal">Back to first</button>
                            </div>
                          </div>
                        </div>
                    </div>
                    <a class="btn position" data-bs-toggle="modal" href="#exampleModalToggle" role="button"><img src="<?php echo get_template_directory_uri(); ?>/images/accueil/fluent_dual-screen-group-24-regular.svg" alt=""></a>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 mt-3 mt-md-3 mt-sm-3 galerie-image">
                    <img src="<?php echo  $galerie3; ?>" alt="" class="w-100" height="400px"><!--images/accueil/pexels-rachel-claire-4577430.jpg-->
                    <div class="modal fade" id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalToggleLabel">Image 1</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <img src="<?php echo  $galerie3_modal1; ?>" alt="" class="w-100 h-100"><!--images/accueil/pexels-picha-6210510.jpg-->
                            </div>
                            <div class="modal-footer">
                              <button class="btn btn-orange3 rounded px-4 py-3" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal" data-bs-dismiss="modal">Open second image</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="modal fade" id="exampleModalToggle2" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalToggleLabel2">Image 2</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <img src="<?php echo  $galerie3_modal2; ?>" alt="" class="w-100 h-100"><!--images/accueil/pexels-nishant-aneja-2385654.jpg-->
                            </div>
                            <div class="modal-footer">
                              <button class="btn btn-orange3 rounded px-4 py-3" data-bs-target="#exampleModalToggle" data-bs-toggle="modal" data-bs-dismiss="modal">Back to first</button>
                            </div>
                          </div>
                        </div>
                    </div>
                    <a class="btn position" data-bs-toggle="modal" href="#exampleModalToggle" role="button"><img src="<?php echo get_template_directory_uri(); ?>/images/accueil/fluent_dual-screen-group-24-regular.svg" alt=""></a>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 mt-3 mt-md-3 mt-sm-3 galerie-image">
                    <img src="<?php echo  $galerie4; ?>" alt="" class="w-100" height="400px"><!--images/accueil/pexels-mathew-thomas-906531.jpg-->
                    <div class="modal fade" id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalToggleLabel">Image 1</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <img src="<?php echo  $galerie4_modal1; ?>" alt="" class="w-100 h-100"><!--images/accueil/pexels-picha-6210510.jpg-->
                            </div>
                            <div class="modal-footer">
                              <button class="btn btn-orange3 rounded px-4 py-3" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal" data-bs-dismiss="modal">Open second image</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="modal fade" id="exampleModalToggle2" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalToggleLabel2">Image 2</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <img src="<?php echo  $galerie4_modal2; ?>" alt="" class="w-100 h-100"><!--images/accueil/pexels-nishant-aneja-2385654.jpg-->
                            </div>
                            <div class="modal-footer">
                              <button class="btn btn-orange3 rounded px-4 py-3" data-bs-target="#exampleModalToggle" data-bs-toggle="modal" data-bs-dismiss="modal">Back to first</button>
                            </div>
                          </div>
                        </div>
                    </div>
                    <a class="btn position" data-bs-toggle="modal" href="#exampleModalToggle" role="button"><img src="<?php echo get_template_directory_uri(); ?>/images/accueil/fluent_dual-screen-group-24-regular.svg" alt=""></a>
                </div>
            </div>
            <div class="text-center mt-4"><a><span class="text-white me-3">Suivez nous sur Instagram </span><i class="icofont-instagram"></i></a></div>
        </div>
    </div>
    
<?php get_footer() ;?>