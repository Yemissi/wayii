<?php 

// Template Name: Contact

 ?>
    <div style="background-image:url(<?php echo  get_field("contact_bg_image"); ?>);">
        <?php get_header(); ?>
        <section class="text-white text-center py-5">
            <h1 class="py-5">Contactez-nous</h1>
        </section>
    </div>
    <div class="py-5"></div>
    <div class="py-5"></div>
    <div class="py-5"></div>
    <div class="py-4"></div>
    <div class="d-lg-none d-xl-block">
        <div class="py-4"></div>
    </div>
    <div class=" container position-relative my-5">
        <div class="row">

        <div class="col-sm-11 position-absolute top-100 start-50 translate-middle">
            <div class="card border-0">
                <div class="card-body  px-5">
                    <h5 class="card-title py-2 text-center">Remplissez ce formulaire</h5>
                    <div class="d-flex justify-content-center pb-3"> <div class="line"> </div> </div>
                    
					<?php echo do_shortcode( '[gravityform id="1"]' ); ?>
					
                    <h5 class="card-title pt-5 py-2 text-center">Newsletter</h5>
                    <div class="d-flex justify-content-center pb-3"> <div class="line"> </div> </div>
                    <div class="bg-degrade">
                        <div class="row d-flex justify-content-center">
                            <div class="col-sm-10 py-3 px-4">
                                <p>Nous publions  régulièrement des  astuces voyage et des idées de destination. Trouver l’inspiration</p>
                                <form class="row g-3 d-flex justify-content-center">
                                    <div class="col-sm-8">
                                      <label for="inputPassword2" class="visually-hidden">Password</label>
                                      <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Ex: name@example.com">
                                    </div>
                                </form>
                                <div class="d-flex justify-content-center">
                                    <button type="submit" class="btn btn-light my-3"> <span class="mx-2 color-orange">Envoyer</span> </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h5 class="card-title pt-5 py-2 text-center">Rejoignez-nous</h5>
                    <div class="d-flex justify-content-center pb-3"> <div class="line"> </div> </div>
                    <ul class="list-inline d-flex justify-content-center">
                        <li><a href=""><img src="images/img-article/facebook-logo.png" alt="" class="tall-icon py-3 mx-3 "></a></li>
                        <li><a href=""><img src="images/img-article/instagram.png" alt="" class="tall-icon py-3  mx-3"></a></li>
                        <li><a href=""><img src="images/img-article/twitter.png" alt="" class="tall-icon py-3  mx-3"></a></li>
                    </ul>
                </div>
            </div>
        </div>
      </div>
    </div>
    <div class="py-5"></div>
    <div class="py-5"></div>
    <div class="py-5"></div>
    <div class="py-5"></div>
    <div class="d-lg-none d-xl-block">
        <div class="py-4"></div>
    </div>
	
<?php get_footer() ;?>